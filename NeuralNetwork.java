import java.lang.Math;
import java.util.Random;
import com.amd.aparapi.Kernel;
import com.amd.aparapi.ProfileInfo;
import com.amd.aparapi.Range;

class NeuralNetwork {
	public static double INITIAL_WEIGHT_RANGE = 0.2;
	protected final Random random = new Random();
	protected final double[][] ih_weights;
	protected final double[][] ho_weights;
	protected final double[] input_activation;
	protected final double[] hidden_activation;
	protected final double[] output_activation;
	protected final double[][] last_ih_weight_delta;
	protected final double[][] last_ho_weight_delta;
	protected final double[] input_gradient;
	protected final double[] hidden_gradient;
	protected final double[] output_gradient;

	public static void main(String[] args) {
		//System.setProperty("org.lwjgl.librarypath", new File("native").getAbsolutePath());
		System.out.println("Building network: ");
		NeuralNetwork nn = new NeuralNetwork(2, 4, 3);
		double[][] trainingData = {{0,0}, {0,1}, {1,0}, {1,1}};
		double[][] targetData = {{0,0,0}, {0,1,1}, {0,1,1}, {1,1,0}};
		nn.train(trainingData, targetData, 1000, 0.5, 0.1);
		double[][] predictions = nn.predict(trainingData);
		for(int a=0; a < predictions.length; a++) {
			System.out.print("Prediction " + a + ":");
			for(double b : predictions[a]) {
				System.out.print(b + "\t");
			}
			System.out.println();
		}


		Kernel kernel = new Kernel() {
			@Override public void run() {
			}
		};
		kernel.execute(1024);
		System.out.println("Execution mode = " + kernel.getExecutionMode());
	}

	public NeuralNetwork(int numVisible, int numHidden, int numOutput) {
		// Add visible bias
		numVisible++;

		// Init weight matrix first dimension
		ih_weights = new double[numVisible][numHidden];
		ho_weights = new double[numHidden][numOutput];

		// Init last activations
		input_activation = new double[numVisible];
		hidden_activation = new double[numHidden];
		output_activation = new double[numOutput];

		// Pre-define gradients
		input_gradient = new double[numVisible];
		hidden_gradient = new double[numHidden];
		output_gradient = new double[numOutput];

		// Init last deltas
		last_ih_weight_delta = new double[numVisible][numHidden];
		last_ho_weight_delta = new double[numHidden][numOutput];

		// Init the second dimension of the weight matrix (Unnecessary?)
		for(int i=0; i < numVisible; i++) { ih_weights[i] = new double[numHidden]; }
		for(int j=0; j < numHidden; j++) { ho_weights[j] = new double[numOutput]; }

		// Init activation dimension two
		for(int i=0; i < numVisible; i++) { last_ih_weight_delta[i] = new double[numHidden]; }
		for(int j=0; j < numHidden; j++) { last_ho_weight_delta[j] = new double[numOutput]; }
		
		// Randomly initialize weights
		for(int i=0; i < numVisible; i++) {
			for(int j=0; j < numHidden; j++) {
				ih_weights[i][j] = INITIAL_WEIGHT_RANGE - 2.0*random.nextDouble()*INITIAL_WEIGHT_RANGE;
			}
		}
		for(int j=0; j < numHidden; j++) {
			for(int k=0; k < numOutput; k++) {
				ho_weights[j][k] = 10*INITIAL_WEIGHT_RANGE - 20.0*random.nextDouble()*INITIAL_WEIGHT_RANGE;
			}
		}
	}

	public void train(double[][] examples, double[][] targets, int iterations, double learningRate, double momentum) {
		for(int a=0; a < iterations; a++) {
			double error = 0.0;
			for(int b=0; b < examples.length; b++) {
				forwardPropagate(examples[b], false);
				error += backwardPropagate(targets[b], learningRate, momentum);
			}
			if(a % 100 == 0) {
				System.out.println("Error at iter " + a + ": " + error);
			}
		}
	}

	public double[][] predict(double[][] inputs) {
		double[][] predictions = new double[inputs.length][];
		for(int a=0; a < inputs.length; a++) {
			predictions[a] = forwardPropagate(inputs[a]);
		}
		return predictions;
	}

	public double[] forwardPropagate(double[] inputs) {
		return forwardPropagate(inputs, true);
	}

	public double[] forwardPropagate(double[] inputs, boolean allocateNewArray) {
		assert(inputs.length == ih_weights.length);

		// "Calculate" input activation
		for(int i=0; i < inputs.length; i++) {
			input_activation[i] = inputs[i];
		}
		input_activation[inputs.length] = 1.0; // Add the bias to the last input

		// Calculate the hidden activation
		for(int j=0; j < hidden_activation.length; j++) {
			double accumulator = 0.0;
			for(int i=0; i < input_activation.length; i++) {
				accumulator += input_activation[i]*ih_weights[i][j];
			}
			hidden_activation[j] = activation(accumulator);
		}

		// Calculate the output activation
		for(int k=0; k < output_activation.length; k++) {
			double accumulator = 0.0;
			for(int j=0; j < hidden_activation.length; j++) {
				accumulator += hidden_activation[j]*ho_weights[j][k];
			}
			output_activation[k] = activation(accumulator);
		}

		if(!allocateNewArray) {
			return output_activation;
		} else {
			double[] output = new double[output_activation.length];
			System.arraycopy(output_activation, 0, output, 0, output_activation.length);
			return output;
		}
	}

	public double backwardPropagate(double[] target, double learningRate, double momentum) {
		// Get output gradient
		for(int k=0; k < output_activation.length; k++) {
			output_gradient[k] = delta_activation(output_activation[k]) * (target[k]-output_activation[k]);
		}

		// Calculate hidden gradient
		for(int j=0; j < hidden_activation.length; j++) {
			double error = 0.0;
			for(int k=0; k < output_activation.length; k++) {
				error += output_gradient[k] * ho_weights[j][k];
			}
			hidden_gradient[j] = delta_activation(hidden_activation[j]) * error;
		}

		// Update hidden->output weights
		for(int j=0; j < hidden_activation.length; j++) {
			for(int k=0; k < output_activation.length; k++) {
				double weight_change = ((output_gradient[k] * hidden_activation[j]) * learningRate) + (last_ho_weight_delta[j][k]*momentum);
				ho_weights[j][k] += weight_change;
				last_ho_weight_delta[j][k] = weight_change;
			}
		}

		// Update input->hidden weights
		for(int i=0; i < input_activation.length; i++) {
			for(int j=0; j < hidden_activation.length; j++) {
				double weight_change = ((hidden_gradient[j] * input_activation[i]) * learningRate) + (last_ih_weight_delta[i][j]*momentum);
				ih_weights[i][j] += weight_change;
				last_ih_weight_delta[i][j] = weight_change;
			}
		}

		// Calculate error
		double error = 0.0;
		for(int k=0; k < output_activation.length; k++) {
			error += 0.5*(target[k]-output_activation[k])*(target[k]-output_activation[k]);
		}
		return error;
	}

	public static double activation(double x) {
		return 1.0/(1.0+Math.exp(-x));
	}

	public static double delta_activation(double x) {
		return activation(x) * (1.0-activation(x));
	}
}
