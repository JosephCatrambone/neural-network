# README #

### Known Bugs ###

* When momentum is nonzero, the network produces NaN values.

### Requirements ###

Available in requirements.txt.  Short version: numpy.  Python 2.7+

### Who do I talk to? ###

* Me: joseph.catrambone [a] gmail

### License ###

Coffeeware variant attribution/academic license.  Use this for what you want as long as you don't hold me liable for anything.  If it's in something commercial, give me a shout out and, at your discretion, buy me a coffee if we meet in person.