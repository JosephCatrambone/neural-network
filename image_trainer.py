#!/usr/bin/env python
from __future__ import print_function
import sys, os
from PIL import Image, ImageDraw, ImageFont
from neural_network import NeuralNetwork
import numpy
import pickle as pickle
import random
from glob import glob

# Image distortion/noise methods
IMAGE_DISTORTIONS = list()
def identity(img):
	return img.copy()
IMAGE_DISTORTIONS.append(identity)

def noise(img, amount=0.1):
	target = Image.new("".join(img.getbands()), img.size) # Make a copy with same attributes, but different pixel data. We'll be touching all the pixels anyway.
	for y in range(img.size[1]):
		for x in range(img.size[0]):
			val = img.getpixel((x,y))
			noise_val = int(255*random.uniform(-amount, amount))
			if isinstance(val, int):
				target.putpixel((x,y), val+noise_val)
			else: # RGB
				target.putpixel((x,y), (val[0]+noise_val, val[1]+noise_val, val[2]+noise_val))
	return target
IMAGE_DISTORTIONS.append(noise)

def rotate(img, max_angle=5.0, expand=False):
	target = img.copy()
	target.rotate(angle=random.uniform(-max_angle, max_angle), expand=expand)
	return target
IMAGE_DISTORTIONS.append(rotate)

def scale(img, max_change=0.3):
	scale_factor = random.uniform(1.0-max_change, 1.0+max_change)
	return img.resize((int(img.size[0]*scale_factor), int(img.size[1]*scale_factor)))
IMAGE_DISTORTIONS.append(scale)

def crop(img, min_crop_size=0.2):
	target = img.copy()
	scale_size = (1.0-min_crop_size)*random.random() + min_crop_size
	crop_width = int(img.size[0]*scale_size)
	crop_height = int(img.size[1]*scale_size)
	x_position = random.randint(0, img.size[0]-crop_width)
	y_position = random.randint(0, img.size[1]-crop_height)
	return target.crop((x_position, y_position, x_position+crop_width, y_position+crop_height))
IMAGE_DISTORTIONS.append(crop)

def watermark(img):
	pass

# Methods
def generate_mutations(img):
	results = list()
	for mut in IMAGE_DISTORTIONS:
		results.append(mut(img))
	return results

def prepare_training_data(size, path_blob, num_samples):
	examples = list()
	targets = list()
	# Image to array: numpy.array(img.convert("L"))
	# Array to image: Image.fromarray(arr)
	for i in range(num_samples):
		filename = random.choice(path_blob)
		img = Image.open(filename)
		# TODO: Preserve aspect-ratio for the resize
		target = (numpy.reshape(numpy.array(img.convert('L').resize((size, size))), (1, -1), order='C')/255.0)[0] # 1, -1 means 1 row, auto-inferr columns
		for mutation in IMAGE_DISTORTIONS:
			example = numpy.reshape(numpy.array(mutation(img).convert('L').resize((size,size))), (1, -1), order='C')
			targets.append(target) # The array or reshape process is leaving result as [[a b c]], so we unwrap it once.
			examples.append((example/255.0)[0]) # This makes it easier to do numpy.asarray and get a 2D matrix.
	return numpy.asarray(examples), numpy.asarray(targets)

def test_network(nn, examples, targets):
	pred = nn.predict(examples)
	target_example_dist = numpy.sum(numpy.abs(targets - examples))
	target_prediction_dist = numpy.sum(numpy.abs(targets - pred))
	example_prediction_dist = numpy.sum(numpy.abs(pred - examples))
	print('"target_example": {0}, "target_prediction\": {1}, "example_prediction": {2}'.format(target_example_dist, target_prediction_dist, example_prediction_dist))
	

def save_network(nn, filename):
	fout = open(filename, 'wb')
	pickle.dump(nn, fout, pickle.HIGHEST_PROTOCOL)
	fout.close()

# Main
if __name__=="__main__":
	# Give args friendly names
	if len(sys.argv) != 5:
		print("Usage: {} [img width/height] [hidden unit count] [output network filename] [image blob]".format(sys.argv[0]))
		sys.exit(0)
	image_size = int(sys.argv[1])
	hidden_units = int(sys.argv[2])
	output_network = sys.argv[3]
	image_path = glob(sys.argv[4])
	batch_size = 1

	# Load the old network first for re-training
	try:
		fin = open(output_network, 'rb')
		nn = NeuralNetwork(image_size*image_size, hidden_units, image_size*image_size)
		nn = pickle.load(fin)
		fin.close()
		print("Loaded network from file {}.".format(output_network))
	except IOError as ioe:
		nn = NeuralNetwork(image_size*image_size, hidden_units, image_size*image_size)
		print("Created new network with {} visible units and {} hidden units.".format(image_size*image_size, hidden_units))

	# Train
	while True:
		print("Preparing training data...")
		examples, targets = prepare_training_data(image_size, image_path, batch_size)
		print("Training...")
		nn.train(numpy.asmatrix(examples), numpy.asmatrix(targets), learning_rate=0.01, momentum=0.3, max_iterations=5000, show_learning=False)
		print("Testing...")
		test_network(nn, examples, targets)
		print("Saving...")
		save_network(nn, output_network)
