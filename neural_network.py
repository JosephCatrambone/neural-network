#!/usr/bin/env python

import numpy

def activate(x):
	#return 1.0/(1.0+numpy.exp(-x)) # Sigmoid
	return numpy.tanh(x)

def delta_activate(x):
	#return numpy.multiply(activate(x), (1.0-activate(x))) # Derivative of sigmoid. This will re-activate the function.  Don't use it.
	#return numpy.multiply(x, 1.0-x) # Since we're passing in the _activation_, we don't have to re-wrap it in activate, otherwise we're double activating it.
	#return 1.0-numpy.power(numpy.tanh(x), 2.0) # Derivative of tangent. Again, this re-runs activation. Use the one below.
	return 1.0 - numpy.power(x, 2.0) # Derivative of activated tangent
	

def add_row_bias(x, value=1):
	"""Adds a bias of 1 to each row."""
	return numpy.concatenate((numpy.ones((x.shape[0], 1)), x)*value, axis=1)

class NeuralNetwork(object):
	def __init__(self, num_inputs, num_hidden, num_outputs, noise_scale=0.001):
		self.num_inputs = num_inputs
		self.num_hidden = num_hidden
		self.num_outputs = num_outputs

		self.wih = numpy.random.uniform(low=-noise_scale, high=noise_scale, size=(num_inputs+1, num_hidden))
		self.who = numpy.random.uniform(low=-noise_scale, high=noise_scale, size=(num_hidden+1, num_outputs))

		self.last_wih_update = numpy.zeros((num_inputs+1, num_hidden))
		self.last_who_update = numpy.zeros((num_hidden+1, num_outputs))

	def train(self, examples, targets, learning_rate=0.0003, momentum=0.1, max_iterations=1000, error_threshold=0.001, show_learning=False):
		error = 1+error_threshold
		while error > error_threshold and max_iterations > 0:
			error = 0
			ia, ha, oa = self.forward_propagate(examples)
			error = self.back_propagate(targets, ia, ha, oa, learning_rate, momentum)
			max_iterations -= 1
			if show_learning and max_iterations % 1000 == 0:
				print("{} - Average error: {}".format(max_iterations, error))

	def predict(self, examples):
		ia, ha, oa = self.forward_propagate(examples)
		return oa

	def cluster(self, training_set, learning_rate=0.05, momentum=0.0, max_iterations=10000, error_threshold=0.0, background_noise=0.0001):
		"""Training set consists of tuples of three parts (input vector A [a tuple or array or matrix], input vector B, distance).
		Distance generally is non-negative.  Zero means the two input examples are similar.  Greater than one means different.
		Set max_iterations to -1 to run until less than error."""
		average_error = error_threshold+1 # So we run once
		while average_error > error_threshold and max_iterations != 0:
			average_error = 0
			for a, b, dist in training_set:
				# Take whatever our network has already.  We don't really care what the output is.
				ia_a, ha_a, out_a = self.forward_propagate(numpy.asmatrix(a))
				ia_b, ha_b, out_b = self.forward_propagate(numpy.asmatrix(b))
				# Find the center between these items in space.
				centerpoint = ((out_a + out_b)/2.0) # Simple average
				center_to_a = out_a - centerpoint
				center_to_b = out_b - centerpoint
				# If the vectors are similar (dist=0) we pull them towards each other. If they're different (greater than one) we push them apart.
				target_vector_a = centerpoint + dist*center_to_a + (background_noise - numpy.random.random(out_a.shape)*2*background_noise) # We add a little noise
				target_vector_b = centerpoint + dist*center_to_b + (background_noise - numpy.random.random(out_b.shape)*2*background_noise)
				average_error += self.back_propagate(numpy.asarray(target_vector_a), ia_a, ha_a, out_a, learning_rate, momentum)
				average_error += self.back_propagate(numpy.asarray(target_vector_b), ia_b, ha_b, out_b, learning_rate, momentum)
				print("Out A: {}\tOut B: {}\tCenter: {}\ttarget_a: {}\ttarget_b: {}".format(out_a, out_b, centerpoint, target_vector_a, target_vector_b))
			max_iterations -= 1

	def distance(self, inputs):
		"""Inputs is a set of tuples (pt1, pt2).  Output is a list of distances between the tuples."""
		inputs_a = numpy.asmatrix([x[0] for x in inputs])
		inputs_b = numpy.asmatrix([x[1] for x in inputs])
		outputs_a = self.predict(inputs_a)
		outputs_b = self.predict(inputs_b)
		return [sum(a*b.T) for a, b in zip(outputs_a, outputs_b)]

	def forward_propagate(self, inputs):
		"""Assumes each row is an input example, adds the bias, and calculates the hidden activation."""
		# Num rows = num examples = shape[0]
		# That means each column of the wih matrix is the connectivity of the inputs to one neuron
		# The output of the wih matrix, then, is the preactivation of the hidden matrix.
		# Add bias terms to inputs and forward-prop
		hidden_values = numpy.asmatrix(add_row_bias(inputs)) * numpy.asmatrix(self.wih) # Numpy.multiply doesn't work here.  Numpy.dot does.  * does.
		hidden_activation = activate(hidden_values)

		# Add bias to hidden and forward prop
		output_values = add_row_bias(hidden_activation) * self.who
		output_activation = output_values # activate(output_values)

		return inputs, hidden_activation, output_activation

	def back_propagate(self, target, input_activation, hidden_activation, output_activation, learning_rate, momentum):
		# Delta rule -> Weight update for neuron b's cth weight, w_bc = learning_rate * (target_b - output_b) * derivative_activation(sum_of_inputs_to_b)*cth_input
		# Calculate output error
		output_error = target-output_activation
		output_delta = output_error #numpy.multiply(output_error, delta_activate(output_activation))

		# Calculate hidden error
		hidden_error = output_delta*self.who.T
		hidden_delta = numpy.multiply(hidden_error, add_row_bias(delta_activate(hidden_activation)))

		# Update hidden->output weight
		hidden_output_delta = (output_delta.T * add_row_bias(hidden_activation)).T
		who_update = hidden_output_delta * learning_rate * (1-momentum) + self.last_who_update * momentum
		self.who += who_update
		self.last_who_update = who_update

		# Update input->hidden weight
		# Note: hidden_delta has activation cut off
		input_hidden_delta = (hidden_delta[:,1:].T * add_row_bias(input_activation)).T
		wih_update = input_hidden_delta * learning_rate * (1-momentum) + self.last_wih_update * momentum
		self.wih += wih_update
		self.last_wih_update = wih_update

		# Weight update input_hidden = input_neuron * error_gradient

		# Add error
		return numpy.sum(numpy.power(output_error, 2))

if __name__=="__main__":
	print("Learning AND, OR, and XOR:")
	examples = numpy.asmatrix([[1, 1], [1, 0], [0, 1], [0, 0]])
	target = numpy.asmatrix([[1, 1, 0], [0, 1, 1], [0, 1, 1], [0, 0, 0]])
	nn = NeuralNetwork(2, 5, 3)
	nn.train(examples, target, learning_rate=0.01, momentum=0.0, max_iterations=50000, show_learning=True)
	print(nn.predict(examples))
